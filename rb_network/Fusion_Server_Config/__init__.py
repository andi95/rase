from socket import AF_INET, AF_INET6, inet_ntop
from ctypes import (
    Structure, Union, POINTER,
    pointer, get_errno, cast,
    c_ushort, c_byte, c_void_p, c_char_p, c_uint, c_int, c_uint16, c_uint32
)
import ctypes.util
import ctypes
import ipaddress
import sys
import time
import random
import struct
import select
import socket

NETMASKS = set(['255.255.255.0', '255.255.255.128', '255.255.255.192',
                '255.255.255.224', '255.255.255.240', '255.255.255.248',
                '255.255.255.252', '255.255.255.255', '255.255.254.0',
                '255.255.252.0', '255.255.248.0', '255.255.240.0',
                '255.255.224.0', '255.255.192.0', '255.255.128.0', '255.255.0.0'])


def get_default_gateway_linux():
    """Read the default gateway directly from /proc."""
    with open("/proc/net/route") as fh:
        for line in fh:
            fields = line.strip().split()
            if fields[1] != '00000000' or not int(fields[3], 16) & 2:
                continue

            return socket.inet_ntoa(struct.pack("<L", int(fields[2], 16)))


def check_correct_address(gateway, netmask, *args):
    """
    Controllo di bontà semantica degli indirizzi di rete e gateway.

        Non sono accettati i seguenti tipi di indirizzi (vedi RFC 5737):
            --> Pubblici
            --> privati multicast
            --> broadcast
            --> indirizzi classe TEST-NET1, TEST-NET2, TEST-NET3
            --> riservati dall'IETF
            --> loopback
            --> Link-local (automatic fallback 169.254/16)

    >>> check_correct_address("193.168.0.1","255.255.255.0","193.168.0.2","193.168.0.3")
    0

    Struttura TASSATIVA e non esemplificativa del args:
        ['gateway', 'netmask', indirizzo1, indirizzo2, ...]
    :param gateway:
    :param netmask:
    :param args:
    :rtype: bool
    """
    exit_code = -1
    if NETMASKS.intersection(set([netmask])) == set():
        sys.exit("Netmask Error.")
    net_gateway = ipaddress.ip_network(gateway + "/" + netmask, False)
    correct_addr = [address.__str__() for address in net_gateway.hosts() if address.__str__() in list(args)]
    for address in correct_addr:
        if not ipaddress.ip_address(address.__str__()).is_private:
            sys.exit("Only private addresses allowed")
    if correct_addr == list(args):
        exit_code = 0
    return exit_code
    pass


class struct_sockaddr(Structure):
    _fields_ = [
        ('sa_family', c_ushort),
        ('sa_data', c_byte * 14), ]


class struct_sockaddr_in(Structure):
    _fields_ = [
        ('sin_family', c_ushort),
        ('sin_port', c_uint16),
        ('sin_addr', c_byte * 4)]


class struct_sockaddr_in6(Structure):
    _fields_ = [
        ('sin6_family', c_ushort),
        ('sin6_port', c_uint16),
        ('sin6_flowinfo', c_uint32),
        ('sin6_addr', c_byte * 16),
        ('sin6_scope_id', c_uint32)]


class union_ifa_ifu(Union):
    _fields_ = [
        ('ifu_broadaddr', POINTER(struct_sockaddr)),
        ('ifu_dstaddr', POINTER(struct_sockaddr)), ]


class struct_ifaddrs(Structure):
    pass


struct_ifaddrs._fields_ = [
    ('ifa_next', POINTER(struct_ifaddrs)),
    ('ifa_name', c_char_p),
    ('ifa_flags', c_uint),
    ('ifa_addr', POINTER(struct_sockaddr)),
    ('ifa_netmask', POINTER(struct_sockaddr)),
    ('ifa_ifu', union_ifa_ifu),
    ('ifa_data', c_void_p), ]

libc = ctypes.CDLL(ctypes.util.find_library('c'))


def ifap_iter(ifap):
    ifa = ifap.contents
    while True:
        yield ifa
        if not ifa.ifa_next:
            break
        ifa = ifa.ifa_next.contents


def getfamaddr(sa):
    family = sa.sa_family
    addr = None
    if family == AF_INET:
        sa = cast(pointer(sa), POINTER(struct_sockaddr_in)).contents
        addr = inet_ntop(family, sa.sin_addr)
    elif family == AF_INET6:
        sa = cast(pointer(sa), POINTER(struct_sockaddr_in6)).contents
        addr = inet_ntop(family, sa.sin6_addr)
    return family, addr


class NetworkInterface(object):
    def __init__(self, name):
        self.name = name
        self.index = libc.if_nametoindex(name)
        self.addresses = {}

    def __str__(self):
        return "%s [index=%d, IPv4=%s, IPv6=%s]" % (
            self.name, self.index,
            self.addresses.get(AF_INET),
            self.addresses.get(AF_INET6))


def get_network_interfaces():
    ifap = POINTER(struct_ifaddrs)()
    result = libc.getifaddrs(pointer(ifap))
    if result != 0:
        raise OSError(get_errno())
    del result
    try:
        retval = {}
        for ifa in ifap_iter(ifap):
            name = ifa.ifa_name
            i = retval.get(name)
            if not i:
                i = retval[name] = NetworkInterface(name)
            family, addr = getfamaddr(ifa.ifa_addr.contents)
            if addr:
                i.addresses[family] = addr
        return retval.values()
    finally:
        libc.freeifaddrs(ifap)


def chk(data):
    x = sum(x << 8 if i % 2 else x for i, x in enumerate(data)) & 0xFFFFFFFF
    x = (x >> 16) + (x & 0xFFFF)
    x = (x >> 16) + (x & 0xFFFF)
    return struct.pack('<H', ~x & 0xFFFF)


def ping(addr, timeout=1, number=1, data=b''):
    with socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_ICMP) as conn:
        payload = struct.pack('!HH', random.randrange(0, 65536), number) + data

        conn.connect((addr, 80))
        conn.sendall(b'\x08\0' + chk(b'\x08\0\0\0' + payload) + payload)
        start = time.time()

        while select.select([conn], [], [], max(0, start + timeout - time.time()))[0]:
            data = conn.recv(65536)
            if len(data) < 20 or len(data) < struct.unpack_from('!xxH', data)[0]:
                continue
            if data[20:] == b'\0\0' + chk(b'\0\0\0\0' + payload) + payload:
                return time.time() - start


if __name__ == '__main__':
    print("\n\n")
    # print(ping('192.168.1.9'))
    print([str(ni) for ni in get_network_interfaces()])
    sys.exit("\nNon dovresti essere qui...")
