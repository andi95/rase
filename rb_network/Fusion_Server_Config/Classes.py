"""
    File script SSLI Server, pensato per la gestione della configurazione del pannello RASE lite.
    Composto da questo unico file ha le seguenti dipendenze:
        --> config.ini file di configurazione che contiene il path relativo da cambiare (si consiglia il config_starter)
        --> cache.ini file di cache con le configurazioni precedenti da usare per vedere cosa cambiare e cosa no
        --> /ro/etc/network/interfaces file di sistema con le info per la configuarazione di rete

"""
import argparse
import configparser
import ipaddress
import os
import shlex
import sys
import subprocess as sub
import textwrap

DNS1 = "8.8.8.8"
DNS2 = "208.67.222.222"

# Console mode.
config = configparser.ConfigParser()
config.read(os.path.join(os.path.dirname(os.path.realpath(__file__)), "config.ini"))
print(os.path.join(os.path.dirname(os.path.realpath(__file__)),"config.ini"))
sys.path.append(config['sys_path']['path'])

from AP_Modifier import check_addr
from Fusion_Server_Config import check_correct_address, get_default_gateway_linux

if "/usr" in config['sys_path']['path']:
    try:
        p = sub.Popen(['mount', '-o', 'remount,rw', '/ro'])
        error, output = p.communicate()
        if error is not None:
            sys.exit("Can't write the config.ini file")
    except Exception as e:
        sys.exit("Error in system shell" + str(e))
    interface_path = "/ro/etc/network/interfaces"
else:
    interface_path = "/etc/network/interfaces"

from api_file.context_api import IFNAME

config = configparser.ConfigParser()
config.read(os.path.join(os.path.dirname(os.path.realpath(__file__)), "cache.ini"))
DEFAULT_INTERFACE = config['default_interface']['nic']  # Variabile che contiene i vecchi valori


def write_file():
    """
    Costruttore del file Fusion_Config
    """
    try:
        file = open(interface_path, "w")
        IPADDR = ipaddress.ip_network(args.indirizzo + "/" + args.netmask, False)
    except Exception as e:
        sys.exit("Bad error")
    # Impacchetto i dati ottentuti dalla WebApp
    config = "\nauto lo \niface lo inet loopback" + \
             "\n\nauto  " + IFNAME.decode() + "  \niface  " + IFNAME.decode() + "  inet static" + \
             "\n     address " + args.indirizzo + \
             "\n     network " + str(IPADDR.network_address) + \
             "\n     netmask " + str(IPADDR.netmask) + \
             "\n     broadcast " + str(IPADDR.broadcast_address) + \
             "\n     gateway " + args.gateway + \
             "\n     dns-nameserver " + DNS1 + " " + DNS2

    # Scrivo i dati impacchettati
    try:
        file.write(config)
    except Exception:
        sys.exit("Errore nella scrittura dei file di configurazione")
    finally:
        file.close()
    print("Setting addresses for application server")
    var = "{0}{1}{2}".format("ifconfig  " + IFNAME.decode() + "  {}".format(args.indirizzo),
                             " netmask {}".format(IPADDR.netmask),
                             " broadcast {}".format(IPADDR.broadcast_address))
    lista = shlex.split(var)
    try:
        p = sub.Popen(lista)
        output, error = p.communicate()
    except Exception:
        sys.exit("Error in command execution")
    var = "route add default gw " + args.gateway
    lista = shlex.split(var)
    try:
        sub.Popen(lista).communicate()
    except Exception:
        sys.exit("Error launching the command")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog='SSLI Server',
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=textwrap.dedent('''\
            ...                 Script SSLI Server
            ...         --------------------------------
            ...             Scrit della manipolazione
            ...             interfaccia rete SSLI server
            ...         '''))

    parser.add_argument('-i', '--indirizzo',
                        help="Indirizzo nuovo da impostare",
                        type=check_addr)

    parser.add_argument('-n', '--netmask',
                        help="Maschera di rete",
                        type=check_addr)

    parser.add_argument('-g', '--gateway',
                        help="Default gateway della rete",
                        type=check_addr)

    parser.add_argument('-d', '--dns1',
                        help="Nuovo indirizzo del primo dns",
                        type=check_addr)

    parser.add_argument('-D', '--dns2',
                        help="Nuovo indirizzo del secondo dns",
                        type=check_addr)

    parser.add_argument('-q', '--query',
                        help="Interroga per sapere i parametri",
                        action='store_true', )

    try:
        args = parser.parse_args()
    except argparse.ArgumentError:
        sys.exit("Errore nel parsing degli argomenti")

    if args.query:
        config = configparser.ConfigParser()
        config.read(os.path.join(os.path.dirname(os.path.realpath(__file__)), "cache.ini"))
        DEFAULT_INTERFACE = config['default_interface']['nic']  # Variabile che contiene i vecchi valori
        try:
            os.chdir(os.path.dirname(os.path.realpath(__file__)))
            result = sub.check_output(['./ipaddr'])
            var = result.decode()
            var = [res for res in var.split("Interface") if config[DEFAULT_INTERFACE]['name'] in res]
            for item in var:
                info = item.split("\n")
                ip_addr = [token.strip("Ip address: ") for token in info if "Ip address" in token]
                netmask = [token.strip("Netmask: ") for token in info if "Netmask" in token]
            print("ip=" + ip_addr[0] + "_^_netmask=" + netmask[
                0] + "_^_gateway=" + get_default_gateway_linux() + "_^_DNS1=" + config[DEFAULT_INTERFACE][
                      'dns1'] + "_^_DNS2=" + config[DEFAULT_INTERFACE]['dns1'])
        except Exception as e:
            sys.exit("Error launching the command \n " + str(e))

        sys.exit(0)

    if args.indirizzo is None:
        args.indirizzo = config[DEFAULT_INTERFACE]['address']
    else:
        config[DEFAULT_INTERFACE]['address'] = args.indirizzo

    if args.gateway is None:
        args.gateway = config[DEFAULT_INTERFACE]['gateway']
    else:
        config[DEFAULT_INTERFACE]['gateway'] = args.gateway

    if args.netmask is None:
        args.netmask = config[DEFAULT_INTERFACE]['netmask']
    else:
        config[DEFAULT_INTERFACE]['netmask'] = args.netmask

    if check_correct_address(args.gateway, args.netmask, args.indirizzo) == -1:
        sys.exit("Wrong address/netmask combination, insert again.")

    if args.dns1 is not None:
        DNS1 = args.dns1
        config[DEFAULT_INTERFACE]['dns1'] = args.dns1

    if args.dns2 is not None:
        DNS2 = args.dns2
        config[DEFAULT_INTERFACE]['dns2'] = args.dns2

    with open(os.path.join(os.path.dirname(os.path.realpath(__file__)), "cache.ini"), 'w') as configfile:
        config.write(configfile)

    write_file()
    print("Configuration terminated")
