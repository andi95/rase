"""
    FILE DELLA WDS CHE SI OCCUPA DEL REPERIRE LE INFO RIGUARDANTI LA WIFI E IMPOSTA LA WDS
"""
# todo: inserire possibilità di aggiungere una route al cloud core e all'AP
# todo: creare uno script di configurazione iniziale per impostare banda e frequenza

# Per sapere le route io devo fare un ping dell'interfaccia wireless e vedere
# quali indirizzi ci sono per poter ricavarmi l'indirizzo della sottorete
#


import argparse
import configparser
import fcntl
import inspect
import ipaddress
import random
import re
import socket
import struct
import sys
config = configparser.ConfigParser()
config.read("config.ini")
sys.path.append(config['sys_path']['path'])
import textwrap
import time
from threading import Thread

from api_file import mndp_scan, get_ip_address, ping
from api_file.context_api import Context_Api, ADDRESS as CR_ADDRESS, AP_ADDRESS as ADDRESS, IFNAME


def myfunc(switch):
    interfaces = ('lte1', 'bridge1') # Da cambiare in combo1 in produzione
    with Context_Api(CR_ADDRESS) as api:  # Controllo che ci sia la WDS
        response = api.talk(["/ip/dhcp-client/print", "?disabled=false"])
        if "!trap" in response[0]:
            raise RuntimeError("Command not executed " + str(response))
        elif "!re" in response[0]:
            if response[0][1]['=interface'].startswith('ether'):
                interfaces = ('ether1', 'bridge1')
        elif "!done" in response[0]:
            interfaces = ('ether1', 'bridge1')  # Indirizzo statico
        interfaces_id = []
        response = api.talk(["/routing/igmp-proxy/interface/print"])
        for item in response:
            if "!re" in item:
                pass
            if "!done" in item:
                break
            interfaces_id.append(item[1]["=.id"])
        for item in interfaces_id:
            api.talk(["/routing/igmp-proxy/interface/remove", "=.id={!s}".format(item)])
        api.talk(["/routing/igmp-proxy/interface/add", "=interface=all"])
        if switch == 'True':
            # abilito igmp proxy upstream su bridge
            response1 = api.talk(
                ["/routing/igmp-proxy/interface/add", "=interface={!s}".format(interfaces[1]), "=upstream=yes"])
            if "!trap" in response1[0]:
                raise RuntimeError("Command not executed {!s}".format(response))
            elif "!re" in response[0]:
                try:
                    response2 = api.talk(["/routing/igmp-proxy/interface/add", "=interface={!s}".format(interfaces[0])])
                    if "!trap" in response2[0]:
                        raise RuntimeError("Command not executed {!s}".format(response2))
                    else:
                        pass
                except Exception as e:
                    print(e)

        else:
            # abilito igmp proxy upstream su bridge
            response1 = api.talk(["/routing/igmp-proxy/interface/add", "=interface=" + interfaces[0], "=upstream=yes"])
            if "!trap" in response1[0]:
                raise RuntimeError("Command not executed {0}".format(str(response)))

            response2 = api.talk(["/routing/igmp-proxy/interface/add", "=interface=" + interfaces[1]])
            if "!trap" in response2[0]:
                raise RuntimeError("Command not executed {0}".format(str(response)))


def parse_args(channel):
    try:
        freq_band, chan_prot = channel.split("-")
        frequency, width = freq_band.split("/")
        extension, band = chan_prot.split("/", 1)
    except ValueError:
        extension = ""
        frequency, width, band = channel.split("/")
    return frequency, width, band, extension


def check_param(string: str):
    """
        Error checking della stringa, lunghezza, injection,
        validità e complessità della uid/pass e quant'altro
    :type string: str
    :rtype: bool
    :param string:
    :return:
    """
    flag = True
    # TODO: fare errro checking per controllora la validità
    if len(string) < 8:
        flag = False
        msg = "%r Stringa troppo corta!" % string
        raise ValueError(msg)
    param = re.sub("[^A-Za-z0-9_]", "", string)[:20]
    if param != string:
        flag = False
        msg = "%r Stringa non valida!" % string
        raise ValueError(msg)
    if flag is False:
        raise ValueError("Errore nella stringa")
    else:
        return param


def get_ip_address(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    response = socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', ifname[:15])
    )[20:24])
    return response


def check_connected():
    response = mndp_scan()
    result = []
    for item in response:
        result.append(item[2][0])
    # todo:gestire altre netmask
    result = sorted(result)
    addr_list = []  # Lista delle sottoreti che non sono la mia
    local_addr = str(ipaddress.ip_network(get_ip_address(IFNAME) + "/24", False))
    for idx in range(len(result) - 1):
        network_addr = str(ipaddress.ip_network(result[idx] + "/24", False))
        if local_addr != network_addr:
            addr_list.append(network_addr)
    addr_list = list(set(addr_list))

    for i in range(3):
        with Context_Api(ADDRESS) as api:  # Controllo che ci sia la WDS
            response = api.talk(["/interface/print", "?type=wds"])
            if "!trap" in response[0]:
                raise RuntimeError("Command not executed " + str(response))
            elif "!done" in response[0]:
                time.sleep((2 ** i) + (random.randint(0, 1000) / 1000))
            elif "!re" in response[0]:
                for item in addr_list:
                    address = str(item).rsplit(".", 1)
                    for i in range(4):
                        if ping(address[0] + ".2") is not None:
                            return "CONNECTED"
                        else:
                            time.sleep((2 ** i) + (random.randint(0, 1000) / 1000))
    else:
        return "Error: can't ping {0} hosts".format(str(args.SSID))


def scan():
    wifi_list = []
    with Context_Api(ADDRESS) as api:
        response = api.talk(["/interface/wireless/scan", "=number=wlan1", "=duration=4"])
    if "!trap" in response[0]:
        raise RuntimeError("Command not executed " + str(response))
    elif "!done" in response[0]:
        raise RuntimeError("No wifi networks found")
    elif "!re" in response[0]:
        for item in response:
            if item[0] == '!done':
                break
            dictionary_resp = item[1]
            ssid = dictionary_resp['=ssid']
            channel = parse_args(dictionary_resp['=channel'])
            try:
                dictionary_resp['=routeros-version']
                routeros = True
            except KeyError:
                routeros = False
                # todo:mettere la possibilità di vedere se è collegato
            # connected = check_connected()
            string = ' frequency="' + channel[0] + '" width="' \
                     + channel[1] + '" band="' + channel[2] + '" extension="' + channel[3] + '"' + ' connected="False"'
            wifi_list.append('ssid="' + str(ssid) + '"' + string + ' Routerboard="' + str(routeros) + '"_^_')
        result = list(set(wifi_list))
        return ''.join(result)


def change_route_status(switch):
    """
    Funzione che in base allo switch disabilita o abilita le varie route del CCR
    :param switch:
    :return:
    """
    subnet_number = str(get_ip_address(IFNAME)).split(".")[2]
    with Context_Api(CR_ADDRESS) as api:

        ccr_route_id_wds = []  # Route della WDS
        # todo:cambia il 9 in subnet_number
        response = api.talk(["/ip/route/print", "?gateway=192.168.{!s}.3".format(subnet_number)])
        if "!trap" in response[0]:
            raise RuntimeError("Command not executed " + str(response))
        elif "!re" in response[0]:
            resp_list = response[1]
            for item in resp_list:
                if "!done" in item:
                    break
                if "!re" in item:
                    continue
                ccr_route_id_wds.append(item['=.id'])

        ccr_route_id_vpn = []
        for i in range(1, 11):
            response = api.talk(["/ip/route/print", "?gateway=10.10.11." + str(i)])
            if "!trap" in response[0]:
                raise RuntimeError("Command not executed " + str(response))
            elif "!re" in response[0]:
                resp_list = response[0]
                ccr_route_id_vpn.append(resp_list[1]['=.id'])

        response = api.talk(["/routing/igmp-proxy/interface/print", "?interface=bridge1"])
        if "!trap" in response[0]:
            raise RuntimeError("Command not executed " + str(response))
        elif "!re" in response[0]:
            bridge_id = response[0][1]['=.id']

        response = api.talk(["/routing/igmp-proxy/interface/print", "?interface=lte1"])
        if "!trap" in response[0]:
            raise RuntimeError("Command not executed " + str(response))
        elif "!re" in response[0]:
            combo_id = response[0][1]['=.id']

        if switch == 'False':  # Disabilito le route della WDS e abilito VPN
            for vpn_route in ccr_route_id_vpn:
                api.talk(["/ip/route/set", "=disabled=no", "=.id={0}".format(str(vpn_route))])

        else:
            for vpn_route in ccr_route_id_vpn:
                api.talk(["/ip/route/set", "=disabled=yes", "=.id={0}".format(str(vpn_route))])


class WDS:
    """
    Compito di questa classe:
        1. Configurare l'access point al quale sono collegato via API
        2. Mettere il dhcp client
        3. Poi pingare grazie all'indirizzo ottenuto i primi 3 indirizzi
        >>> mikrotik = WDS()
        >>> vars(mikrotik) != None
        >>> True

    """

    @property
    def mode(self):
        return self._mode

    @property
    def wds_mode(self):
        if self._wds_mode == "disabled":
            return "off"
        elif self._wds_mode == "dynamic":
            return "on"
        else:
            return "Invalid WDS state"

    def __init__(self):
        """
        Funzioe che inizializza l'access point ritrovando i dati dall'AP
        :rtype: object
        """
        with Context_Api(ADDRESS) as api:
            response = api.talk(["/interface/wireless/print"])
            password = api.talk(["/interface/wireless/security-profiles/print"])

        if "!trap" in response[0]:
            raise RuntimeError("Command not executed " + str(response))
        elif "!re" in response[0]:
            dictionary = response[0][1]
            self._ssid = dictionary['=ssid']
            self._channel_width = dictionary['=channel-width']
            self._frequency = dictionary['=frequency']
            self._band = dictionary['=band']
            self._mode = dictionary['=mode']
            self._wds_mode = dictionary['=wds-mode']
            self._uid = dictionary['=.id']
            self._server_client = dictionary['=mode']
        else:
            raise ValueError("Malformed wifi info packet received")

        if "!trap" in password[0]:
            raise RuntimeError("Command not executed " + str(response))
        elif "!re" in password[0]:
            dictionary = password[0][1]
            if dictionary['=mode'] != "none":
                self._password = dictionary['=wpa2-pre-shared-key']
            else:
                self._password = ""
        else:
            raise ValueError("Malformed password packet received")

    def config(self, switch, wifi_dictionary):
        change_route_status(switch)
        if switch == 'True' and bool(wifi_dictionary):
            # Attivo la WDS
            # Dato che mi conneto lo metto come wds-slave
            self._server_client = "wds-slave"
            self._ssid = wifi_dictionary['ssid']
            self._password = wifi_dictionary['password']
            # self._frequency, self._band, \
            # self._channel_width = parse_args(wifi_dictionary['channel'])
            self._wds_mode = "dynamic"  # active WDS
            with Context_Api(ADDRESS) as api:
                for i in range(4):
                    response = api.talk(["/interface/print", "?type=wds"])
                    if "!trap" in response[0]:
                        raise RuntimeError("Command not executed " + str(response))
                    elif "!done" in response[0]:
                        time.sleep((2 ** i) + (random.randint(0, 1000) / 1000))
                    elif "!re" in response[0]:
                        break
                else:
                    self._mode = "station-wds"
                    time.sleep(0.5)
                    self._mode = 'wds-slave'
                    for i in range(4):
                        response = api.talk(["/interface/print", "?type=wds"])
                        if "!trap" in response[0]:
                            raise RuntimeError("Command not executed " + str(response))
                        elif "!done" in response[0]:
                            time.sleep((2 ** i) + (random.randint(0, 1000) / 1000))
                        elif "!re" in response[0]:
                            break
                    else:
                        self._server_client = "station-wds"
                        if check_connected() != None:
                            print("Connection status OK in station WDS mode.")
                        sys.exit("Warning: DHCP services may not work properly, switch to static addresses in your devices.")

                return check_connected()
        elif switch == 'True':
            self._wds_mode = "dynamic"
            self._mode = "ap-bridge"
        elif switch == 'False':
            # Disattivo la WDS
            self._wds_mode = "disabled"  # disabled WDS
            self._mode = "ap-bridge"
        else:
            raise ValueError("Malformed switching WDS request")

    @property
    def uid(self):
        return self._uid

    @property
    def frequency(self):
        return self._frequency

    @property
    def band(self):
        return self._band

    @property
    def ssid(self):
        return self._ssid

    @property
    def password(self):
        return self._password

    @property
    def channel_width(self):
        return self._channel_width

    @property
    def server_client(self):
        if self._server_client == "ap-bridge":
            return "server"
        elif self._server_client == "wds-slave" or self._server_client == "station-wds":
            return "client"
        else:
            return "Error in wds-mode"

    def __setattr__(self, key, value):
        if inspect.stack()[1][3] == '__init__':
            object.__setattr__(self, key, value)
        else:
            if str(key).startswith("_"):
                key = key[1:]
            if key == "channel_width":
                str(key).replace("_", "-")
            if key == "wds_mode":
                str(key).replace("_", "-")
            if key == "server_client":
                key = "mode"
            if key != 'password':
                with Context_Api(ADDRESS) as api:
                    response = api.talk(["/interface/wireless/set", "=.id=" + self.uid,
                                         "={0}={1}".format(str(key).replace("_", "-"), value)])
                if "!trap" in response[0]:
                    raise RuntimeError("Command not executed " + str(response))
                elif "!done" in response[0]:
                    pass
            else:
                with Context_Api(ADDRESS) as api:
                    response = api.talk(["/interface/wireless/security-profiles/print"])
                if "!trap" in response[0]:
                    raise RuntimeError("Command not executed " + str(response))
                elif "!done" in response[0]:
                    raise RuntimeError("No security profiles found, can't set password")
                elif "!re" in response[0]:
                    for item in response:
                        if item[0] == "!done":
                            break
                        dictionary = item[1]
                        if dictionary['=wpa2-pre-shared-key'] != '':
                            uid = dictionary['=.id']
                            with Context_Api(ADDRESS) as api:
                                response2 = api.talk(["/interface/wireless/security-profiles/set",
                                                      "=.id=" + uid,
                                                      '=wpa2-pre-shared-key=' + value])
                                if "!trap" in response2[0]:
                                    raise RuntimeError("Command not executed " + str(response2))


if __name__ == '__main__':
    # print(check_connected())
    if ping(ADDRESS) is None:
        sys.exit("Access Point not reachable")
    # todo: aggiungi il supporto alla maodalità AP-bridge
    parser = argparse.ArgumentParser(
        prog='AP_Modifier',
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=textwrap.dedent('''\
            ...            Script Access Point modifier
            ...         --------------------------------
            ...             Scrit della manipolazione
            ...             WIFI con Access Point.
            ...         '''))

    parser.add_argument('-g', '--getparams',
                        action="store_true",
                        help="Esegui la scansione")

    parser.add_argument('-m', '--mode',
                        action="store_true",
                        help="Get dello status iniziale ON/OFF")

    parser.add_argument('-c', '--make_server',
                        help="Settalo come client 2 come server 1")

    parser.add_argument('-o', '--onoff',
                        help="Abilita/Disabilita WDS",
                        required=False,
                        )

    parser.add_argument('-s', '--SSID',
                        help="SSID della WIFI",
                        required=False,
                        type=check_param)

    parser.add_argument('-p', '--password',
                        help="Password della WIFI",
                        required=False,
                        type=check_param)

    parser.add_argument('-q', '--query_ssid_pw',
                        action="store_true",
                        help="Query per ottenere SSID e password")

    try:
        args = parser.parse_args()
    except Exception as e:
        sys.exit("Error parsing arguments")
    mikrotik = WDS()

    try:
        if args.SSID is not None and args.password is not None:
            wifi_dictionary = {'ssid': args.SSID, 'password': args.password}
            mikrotik.ssid = args.SSID
            mikrotik.password=args.password
        elif args.SSID is not None and args.password is None:
            wifi_dictionary = {'ssid': args.SSID, 'password': ""}
            mikrotik.ssid = args.SSID
        elif args.SSID is None and args.password is not None:
            wifi_dictionary = {'ssid': args.SSID, 'password': ""}
            mikrotik.password = args.password
        else:
            wifi_dictionary={}
    except Exception as e:
        print("Error {0}".format(str(e)))  # principi DRY infranto!! CORREGGERE

    try:
        if args.onoff is not None:
            t = Thread(target=myfunc, args=(args.onoff,))
            t.start()
            print('connected=\"{0}\"'.format(str(mikrotik.config(args.onoff, wifi_dictionary))))
            t.join(2)

    except AttributeError as e:
        if str(e) == "'ArgumentParser' object has no attribute 'onoff'":
            print("No on/off flag received")
        else:
            raise AttributeError(str(e))

    if args.getparams is True:
        result = scan()
        print(result)
    try:
        if args.make_server == "1":
            # Vuol dire che io voglio mettere questo access point come server
            mikrotik.server_client = "ap-bridge"  # Codice per metterlo server
            print("WDS mode now server")
        elif args.make_server == "2":
            mikrotik.server_client = "station-wds"
            print("WDS mode now client")
    except Exception:
        pass

    if args.mode is True:
        print('status="' + mikrotik.wds_mode + '"' + '_^_mode="' + mikrotik.server_client + '"')

    if args.query_ssid_pw is True:
        print('ssid="' + mikrotik.ssid + '"_^_password="' + mikrotik.password + '"')
