"""FILE CONTENTE LA CLASSE CONTEXT MANAGER DELLA CONNESSIONE API
   PRESTA MOLTA ATTENZIONE AL PEP 443 VEDI ANCHE 
   http://stackoverflow.com/questions/39611520/init-vs-enter-in-context-managers"""
import hashlib
import socket
import struct
import binascii
import os
import time
import sys

from rb_network.config import USERNAME, PASSWORD

from api_file import get_ip_address

iface_list = os.listdir('/sys/class/net/')
IFNAME = bytes([iface for iface in iface_list if 'lo' != iface][0], 'utf-8')

if os.isatty(sys.stdin.fileno()):
    # Console mode.
    sys.path.append("/ro/usr/local/bin/rb_network")
    AP_ADDRESS = str(get_ip_address(IFNAME)).rsplit(".", 1)[0] + ".3"
    ADDRESS = str(get_ip_address(IFNAME)).rsplit(".", 1)[0] + ".2"

else:
    # IDE debug mode.
    # AP_ADDRESS = str(get_ip_address(IFNAME)).rsplit(".", 1)[0] + ".3"
    ADDRESS = "192.168.232.128 "


def chk(data):
    x = sum(x << 8 if i % 2 else x for i, x in enumerate(data)) & 0xFFFFFFFF
    x = (x >> 16) + (x & 0xFFFF)
    x = (x >> 16) + (x & 0xFFFF)
    return struct.pack('<H', ~x & 0xFFFF)

class Context_Api(object):
    def __init__(self, address):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect((address, 8728))

    def __enter__(self):
        self.login(USERNAME, PASSWORD)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        try:
            self.talk(["/quit"])
        except RuntimeError as e:
            if str(e) == "connection closed by remote end":
                pass
            else:
                raise ConnectionError(e)
        finally:
            self.sock.close()

    def __del__(self):
        try:
            self.handler.talk(["/quit"])
        except RuntimeError:
            print("Connection closed")
        except AttributeError:
            pass
        except Exception as e:
            print(e)
        finally:
            self.sock.close()

    @property
    def handler(self):
        return self._handler

    @handler.setter
    def handler(self, value):
        if value is not None:
            self._handler = value
        else:
            print("Null API")

    def login(self, username, pwd):
        for repl, attrs in self.talk(["/login"]):
            chal = binascii.unhexlify(attrs['=ret'])
        md = hashlib.md5()
        md.update(b'\x00')
        md.update(bytes(pwd, 'utf-8'))
        md.update(chal)
        self.talk(["/login", "=name=" + username,
                   "=response=00" + binascii.hexlify(md.digest()).decode('utf-8')])

    def talk(self, words):
        if self.writeSentence(words) == 0: return
        r = []
        while 1:
            i = self.readSentence();
            if len(i) == 0: continue
            reply = i[0]
            attrs = {}
            for w in i[1:]:
                j = w.find('=', 1)
                if (j == -1):
                    attrs[w] = ''
                else:
                    attrs[w[:j]] = w[j + 1:]
            r.append((reply, attrs))
            if reply == '!done': return r

    def writeSentence(self, words):
        ret = 0
        for w in words:
            self.writeWord(w)
            ret += 1
        self.writeWord('')
        return ret

    def readSentence(self):
        r = []
        while 1:
            w = self.readWord()
            if w == '': return r
            r.append(w)

    def writeWord(self, w):
        # print("<<< " + w)
        b = bytes(w, "utf-8")
        self.writeLen(len(b))
        self.writeBytes(b)

    def readWord(self):
        ret = self.readBytes(self.readLen()).decode('utf-8')
        # print(ret)
        return ret

    def writeLen(self, l):
        if l < 0x80:
            self.writeBytes(bytes([l]))
        elif l < 0x4000:
            l |= 0x8000
            self.writeBytes(bytes([(l >> 8) & 0xff, l & 0xff]))
        elif l < 0x200000:
            l |= 0xC00000
            self.writeBytes(bytes([(l >> 16) & 0xff, (l >> 8) & 0xff, l & 0xff]))
        elif l < 0x10000000:
            l |= 0xE0000000
            self.writeBytes(bytes([(l >> 24) & 0xff, (l >> 16) & 0xff, (l >> 8) & 0xff, l & 0xff]))
        else:
            self.writeBytes(bytes([0xf0, (l >> 24) & 0xff, (l >> 16) & 0xff, (l >> 8) & 0xff, l & 0xff]))

    def readLen(self):
        c = self.readBytes(1)[0]
        if (c & 0x80) == 0x00:
            pass
        elif (c & 0xC0) == 0x80:
            c &= ~0xC0
            c <<= 8
            c += self.readBytes(1)[0]
        elif (c & 0xE0) == 0xC0:
            c &= ~0xE0
            c <<= 8
            c += self.readBytes(1)[0]
            c <<= 8
            c += self.readBytes(1)[0]
        elif (c & 0xF0) == 0xE0:
            c &= ~0xF0
            c <<= 8
            c += self.readBytes(1)[0]
            c <<= 8
            c += self.readBytes(1)[0]
            c <<= 8
            c += self.readBytes(1)[0]
        elif (c & 0xF8) == 0xF0:
            c = self.readBytes(1)[0]
            c <<= 8
            c += self.readBytes(1)[0]
            c <<= 8
            c += self.readBytes(1)[0]
            c <<= 8
            c += self.readBytes(1)[0]
        return c

    def writeBytes(self, str):
        """
        Funzione che si occupa di 'spedire' veramente i pacchetti
        :param str: 
        :return: 
        """
        n = 0;
        while n < len(str):
            r = self.sock.send(str[n:])
            if r == 0: raise RuntimeError("connection closed by remote end")
            n += r

    def readBytes(self, length):
        ret = b''
        while len(ret) < length:
            s = self.sock.recv(length - len(ret))
            if len(s) == 0: raise RuntimeError("connection closed by remote end")
            ret += s
        return ret


if __name__ == '__main__':
    time_t = time.time()
    with Context_Api(ADDRESS) as api:
        response = api.talk(["/interface/print"])
    print(response)
    print("Time elapsed: {!s}".format(time.time() - time_t))
