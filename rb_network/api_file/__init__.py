import configparser
import fcntl
import ipaddress
import random
import re
import select
import socket
import struct
import time
from binascii import hexlify
from struct import *
import sys

config = configparser.ConfigParser()
config.read("config.ini")
sys.path.append(config['sys_path']['path'])

if "/usr" in config['sys_path']['path']:
    IFNAME="eth0"
else:
    IFNAME="ens33"


def parse_mndp(data):
    entry = {}
    names = ('version', 'ttl', 'checksum')
    for idx, val in enumerate(unpack_from('!BBH', data)):
        entry[names[idx]] = val

    pos = 4
    while pos + 4 < len(data):
        type, length = unpack_from('!HH', data, pos)
        pos += 4

        # MAC
        if type == 1:
            (mac,) = unpack_from('6s', data, pos)
            entry['mac'] = "%02x:%02x:%02x:%02x:%02x:%02x" % tuple(x for x in mac)

        # Identity
        elif type == 5:
            entry['id'] = data[pos:pos + length]

        # Platform
        elif type == 8:
            entry['platform'] = data[pos:pos + length]

        # Version
        elif type == 7:
            entry['version'] = data[pos:pos + length]

        # uptime?
        elif type == 10:
            (uptime,) = unpack_from('<I', data, pos)
            entry['uptime'] = uptime

        # hardware
        elif type == 12:
            entry['hardware'] = data[pos:pos + length]

        # softid
        elif type == 11:
            entry['softid'] = data[pos:pos + length]

        # ifname
        elif type == 16:
            entry['ifname'] = data[pos:pos + length]

        else:
            entry['unknown-%d' % type] = hexlify(data[pos:pos + length])

        pos += length

    return entry


def mndp_scan():
    """
    Test per vedere che la macchina supporti il traffico BROADCAST altrimenti sarebbe un problema!!

    >>> import subprocess
    >>> b" UP BROADCAST RUNNING MULTICAST " in subprocess.Popen(["ifconfig",], stdout=subprocess.PIPE).communicate()[0]
    True

    :return:
    """
    import selectors
    import socket

    import time

    from api_file import parse_mndp
    mysel = selectors.DefaultSelector()
    outgoing = [b'\0\0\0\0',
                b'\0\0\0\0',
                b'\0\0\0\0', ]
    server_address = ('', 5678)
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    sock.bind(server_address)
    sock.setblocking(False)
    mysel.register(
        sock,
        selectors.EVENT_READ | selectors.EVENT_WRITE,
    )
    lista, noduplist = [], []
    time_t = time.time() + 5
    while time.time() < time_t:
        for key, mask in mysel.select(timeout=5):
            connection = key.fileobj
            if mask & selectors.EVENT_READ:
                data, src_addr = connection.recvfrom(1024)
                if data:
                    entries = {}
                    if data == '\0\0\0\0':
                        continue
                    if len(data) < 18:
                        continue
                    entry = parse_mndp(data)
                    if entry['mac'] not in entries:
                        entries['address'] = src_addr
                        entries[entry['mac']] = entry
                        lista.append([entry['hardware'].decode('utf-8'), entry['mac'], entries['address']])

            if mask & selectors.EVENT_WRITE:
                if not outgoing:
                    mysel.modify(sock, selectors.EVENT_READ)
                else:
                    next_msg = outgoing.pop()
                    sock.sendto(next_msg, ('255.255.255.255', 5678))
    mysel.unregister(connection)
    connection.close()
    mysel.close()
    for i in lista:
        if i not in noduplist:
            noduplist.append(i)
    return noduplist


def get_mac(param: str) -> str:
    """
    Funzione che mi stampa a video i MAC address individuati
    :rtype: None
    """
    p = re.compile(r'(?:[0-9a-fA-F]:?){12}')
    a = set(re.findall(p, param))
    return str(a)


def chk(data):
    x = sum(x << 8 if i % 2 else x for i, x in enumerate(data)) & 0xFFFFFFFF
    x = (x >> 16) + (x & 0xFFFF)
    x = (x >> 16) + (x & 0xFFFF)
    return struct.pack('<H', ~x & 0xFFFF)


def ping(addr, timeout=1, number=1, data=b''):
    """
    Implementazione pure python del ping così come stabilito nel RFC 792 usando pacchetto ICMP
    con socket raw evitando uno spawning della console, controllo con questo doctest il semplice
    funzionamento dello stack TCP/IP, se non dovesse funzionare è un casino...

    TIP: Controlla per prima cosa se l'interfaccia importata è quella giusta

    >>> ping('localhost') != None
    True


    :param addr:
    :param timeout:
    :param number:
    :param data:
    :return:
    """
    with socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_ICMP) as conn:
        payload = struct.pack('!HH', random.randrange(0, 65536), number) + data

        conn.connect((addr, 80))
        conn.sendall(b'\x08\0' + chk(b'\x08\0\0\0' + payload) + payload)
        start = time.time()

        while select.select([conn], [], [], max(0, start + timeout - time.time()))[0]:
            data = conn.recv(65536)
            if len(data) < 20 or len(data) < struct.unpack_from('!xxH', data)[0]:
                continue
            if data[20:] == b'\0\0' + chk(b'\0\0\0\0' + payload) + payload:
                return time.time() - start


def check_connected():
    """

    >>> check_connected()
    []

    :return:
    """
    response = mndp_scan()
    my_addr = ipaddress.ip_network(get_ip_address(IFNAME) + "/24", False)
    addr_list = [item[2][0] for item in response
                 if my_addr != ipaddress.ip_network(item[2][0] + "/24", False)]
    res_list = []
    for item in addr_list:
        address = str(item).rsplit(".", 1)
        if ping(address[0] + ".1") is not None:
            res_list.append(address[0] + ".0/24")
    return res_list


def get_ip_address(ifname):
    """
    Funzione che ottiene l'indirizzo IP della macchina tramite SIOCGIFADDR

    >>> get_ip_address(b"ens33")
    '192.168.232.142'

    :param ifname:
    :return:
    """
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    response = socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', ifname[:15])
    )[20:24])
    return response


if __name__ == "__main__":
    addr_list = set([network for network in check_connected()])
    if addr_list != set():
        print("Connected to: " + " ".join(addr_list))
