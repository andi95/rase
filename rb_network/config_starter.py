# File che si occupa dell'allineamento dei file di config
import argparse
import configparser
import os
import textwrap

config = configparser.ConfigParser()

# Packages che dovranno essere configurati
packages = ['AP_Modifier', 'api_file', 'Fusion_Server_Config']

parser = argparse.ArgumentParser(
    prog='Config Starter',
    formatter_class=argparse.RawDescriptionHelpFormatter,
    description=textwrap.dedent('''\
            ...                 Script Configurazione
            ...         --------------------------------
            ...             Script della manipolazione
            ...             e allineamento file config
            ...         '''))

parser.add_argument('-t', action='store_true', default=False,
                    dest='production',
                    help='Imposta la modalità di esercizio,'
                         ' se presente -> production')

args = parser.parse_args()

for root, dirs, files in os.walk(".", topdown=False):
    for name in dirs:
        if name in packages:
            if args.production:
                config['PATH'] = {'path': "/usr/local/bin/rb_network"}
            else:
                config['PATH'] = {'path': "/home/andi/Desktop/RASE/rb_network"}
            with open(os.path.join(os.path.join(root, name), 'config.ini'), 'w') as configfile:
                config.write(configfile)
            print("Modificato in {0}".format(str(os.path.join(root, name))))
