# -*- coding: utf-8 -*-
"""
File che mi restituisce l'handler della connessione Telnet
col router in modo da vere un oggetto rponto da usare

==================================================
ATTENZIONE: NON ESEGUIRE QUESTO FILE STAND-ALONE!
QUESTO È UN FILE DI APPOGGIO, leggi sotto ;)
===================================================
"""
import configparser
import shlex
import socket
import subprocess
import telnetlib
import re
import time

import sys
config = configparser.ConfigParser()
config.read("config.ini")
sys.path.append(config['sys_path']['path'])

from rb_network.config import logger, USERNAME as username, PASSWORD as password


def obtain(address: str) -> telnetlib.Telnet:
    """
    Funzione che si occupa di restituire l'handler della connesisone Telnet

    1. Parsing dei parametri in ingresso
    2. Check_if_Alive
    3. Connessione 
    :rtype: telnetlib.Telnet
    :param address:

    >>> obtain("192.168.232.128").close()

    """

    try:
        socket.inet_aton(address)
    except Exception as e:
        import inspect
        logger.error("Exception in function: {s[1][4]}\n\t--->Error: {0}".format(e, s=inspect.stack()))
        sys.exit(14)

    try:
        handle = telnetlib.Telnet(address, 23, 5)

        if handle.read_until(b"Login: ", 2) is None:
            logger.error("Connection died during login before user input.")
            sys.exit(110)

        handle.write(username.encode('utf-8') + b"\n\r")

        if handle.read_until(b"Password: ", 2) is None:
            logger.error("Connection died during login before password input.")
            sys.exit(110)

        handle.write(password.encode('utf-8') + b'\n\r')
        i = 0
        s = handle.read_until(b"[" + username.encode() + b"@MikroTik] >", 2)
        while s == b'':
            s = handle.read_until(b"[" + username.encode() + b"@MikroTik] >", 2)
            i += 1
            if i > 9:
                logger.error("No connection available. Too many attempts.")
                sys.exit(110)
        if i == 0:
            time.sleep(3)

        return handle

    except Exception as e:
        import inspect
        logger.error("Exception during telnetlib.Telnet({0}{1}".format(address,
                                                                       "): {s[1][4]}\n\t--->Error: {0}".format(e,
                                                                                                               s=inspect.stack())))
        sys.exit(14)


def get_netbit(netmask):
    lookup_table = {
        '/24': "255.255.255.0",
        '/25': "255.255.255.128",
        '/26': "255.255.255.192",
        '/27': "255.255.255.224",
        '/28': "255.255.255.240",
        '/29': "255.255.255.248",
        '/30': "255.255.255.252",
    }
    for k in lookup_table:
        if netmask in lookup_table[k]:
            return k
    raise ValueError("Netmask errata")


def get_gateway():
    """
       Fuzione che si occupa di trovare l'indirizzo del default gateway della macchina
    """
    strs = subprocess.check_output(shlex.split('ip r l'))
    gateway = strs.split('default via'.encode('utf-8'))[-1].split()[0]
    ip = strs.split('src'.encode('utf-8'))[-1].split()[0]
    gw = gateway.decode()
    lista = gw.split("/")
    return lista[0]


def check_param(string: str):
    """
        Error checking della stringa, lunghezza, injection,
        validità e complessità della uid/pass e quant'altro
    :type string: str
    :rtype: bool
    :param string:
    :return:
    """
    flag = True
    # TODO: fare errro checking per controllora la validità
    if len(string) < 8:
        flag = False
        msg = "%r Stringa troppo corta!" % string
        raise ValueError(msg)
    param = re.sub("[^A-Za-z0-9_]", "", string)[:20]
    if param != string:
        flag = False
        msg = "%r Stringa non valida!" % string
        raise ValueError(msg)
    if flag is False:
        raise ValueError("Errore nella stringa")
    else:
        return param


def check_addr(addr):
    """
    Return addr parameter if it's a valid address
    :param addr:
    :return:

    ->Doctest necessario per vedere se c'è la libreria socket
    >>> check_addr("255.255.255.0")
    '255.255.255.0'
    """
    try:
        socket.inet_aton(addr)
    except socket.error as msg:
        raise ValueError("Errore nel\'indirizzo o netmask fornito {0}".format(str(msg)))
    return addr


if __name__ == '__main__':
    print("Inizio...")
