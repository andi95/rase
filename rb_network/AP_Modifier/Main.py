#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
    Docstring I parametri che mi servono in ingresso sono:
        sys.argv[2] SSID
        sys.argv[3] password
        sys.argv[1] indirizzo di rete raggiungibile
        args.address nuovo indirizzo di rete da impostare
        args.netmask nuova netmask da usare
"""
import subprocess as sub
import logging
import sys, os
if os.isatty(sys.stdin.fileno()):
    # Console mode.
    import __init__
else:
    # IDE debug mode.
    from __init__ import check_addr, check_param

import argparse
import textwrap
import time


def set_SSID(handler):
    i = 0
    while i < 4:
        handler.write(b'/interface wireless set 0 ssid="' + args.SSID.encode('utf-8') + b'" \r\n')
        time.sleep(0.5)
        handler.read_until(b"[Andi@MikroTik] >", 2)
        i += 1


def set_PW(handler):
    i = 0
    while i < 4:
        handler.write(b'/interface wireless security-profile set 0 wpa2-pre-shared-key="' +
                      args.password.encode('utf-8') + b'"\r\n')
        time.sleep(0.5)
        handler.read_until(b"[Andi@MikroTik] >", 2)
        i += 1


def set_AP(new_addr, netmask):
    netbit = __init__.get_netbit(netmask)
    i = 0
    add_addr = new_addr + netbit
    while i < 4:
        handler.write(b" /ip address set numbers=[find interface=ether1] address=" + add_addr.encode() + b"\r\n")
        time.sleep(0.5)
        handler.read_until(b"[Andi@MikroTik] >", 2)
        i += 1
    time.sleep(2)
    handler.write(b"/ip dhcp-client release 0\r\n")
    handler.read_until(b"[Andi@MikroTik] > ", 2)


if __name__ == '__main__':
    p = sub.Popen(['mount', '-o', 'remount,rw', '/ro'])
    error, output = p.communicate()
    if error is not None:
        sys.exit("Unable to write file" + error)
    open("/ro/usr/local/bin/rb_network/AP_Modifier/log", 'w').close()
    logger = logging.getLogger('AP_Modifier')
    hdlr = logging.FileHandler('/ro/usr/local/bin/rb_network/AP_Modifier/log')
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.DEBUG)
    parser = argparse.ArgumentParser(
        prog='AP_Modifier',
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=textwrap.dedent('''\
        ...            Script Access Point modifier
        ...         --------------------------------
        ...             Scrit della manipolazione
        ...             WIFI con Access Point.
        ...         '''))

    parser.add_argument('-i', '--indirizzo',
                        help="Indirizzo vecchio dell'AP (raggiungibile)",
                        required=False)

    parser.add_argument('-s', '--SSID',
                        help="SSID della WIFI",
                        required=False,
                        type=check_param)

    parser.add_argument('-p', '--password',
                        help="Password della WIFI",
                        required=False,
                        type=check_param)

    parser.add_argument('-a', '--address',
                        help="Nuovo indirizzo da impostare",
                        type=check_addr)

    parser.add_argument('-n', '--netmask',
                        help="Netmask della rete",
                        type=check_addr)

    try:
        args = parser.parse_args()
    except argparse.ArgumentError:
        logger.exception("Errore nel parsing degli argomenti")

    logger.info(sys.argv)

    # if args.SSID is None or args.password is None or args.indirizzo is None:
    #     logger.error("Nessun parametro passato")
    #     sys.exit("Non ho ricevuto niente")
    try:
        handler = __init__.obtain("192.168.9.3")
    except Exception as e:
        logger.exception("Errore grave nella connessione Telnet {0}".format(str(e)))
        sys.exit("Errore nella connessione col router")
    if args.address is None and args.netmask is None and args.SSID is not None and args.password is not None:
        logger.info("Cambio, SSID password dell'AP")
        print("Changed SSID and password of the Access Point")
        set_SSID(handler)
        set_PW(handler)
    elif args.address is None and args.netmask is not None:
        logger.error("Errore, fornire una entmask valida")
        sys.exit("Error insert a valid netmask")
    elif args.address is not None and args.netmask is None:
        logger.error("Errore, fornire un indirizzo IP valido")
        sys.exit("Error insert a valid IP address")
    elif args.SSID is None and args.password is None:
        logger.info("Cambio solo l'indirizzo dell'AP")
        print("Changed the IP address of the Acccess Point")
        set_AP(args.address, args.netmask)
    elif args.SSID is None and args.password is not None:
        logger.info("Cambio solo la password")
        print("WiFi password changed")
        set_PW(handler)
    elif args.SSID is not None and args.password is None:
        logger.info("Cambio l'SSID dell'AP")
        print("SSID of the wifi changed")
        set_SSID(handler)
    else:
        logger.info("Cambio SSID password e indirizzo dell'AP")
        print("Changed SSID and password of the WiFi")
        set_SSID(handler)
        set_PW(handler)
        # set_AP(args.address, args.netmask)

    handler.close()
    logger.info("Configurazione terminata")
    p = sub.Popen(['mount', '-o', 'remount,ro', '/ro'])
    error, output = p.communicate()
