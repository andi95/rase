"""
SCRIPT DI CONFIGURAZIONE CHE USO PER AVERE I PARAMETRI GLOBALI DELLA RETE. ATTENZIONE DIPENDE STRETTAMENTE DAI FILE:

        --> config.ini
        --> logging.ini
        --> secrets.ini (shadowed)

Questo file viene importato prima dell'eseccuzione degli altri script (richiamati da Matteo) per poter avre username e
password che servono all'esecuzione senza averli scritti nel codice oltre ad avere a configurazione per il logger (che
dovrà essere rivista).
"""
import configparser
import os
from logging.config import fileConfig
import logging

directory = os.path.abspath(os.path.join(os.path.abspath(os.path.join(__file__, os.pardir))))
config = configparser.ConfigParser()
config.read(os.path.abspath(os.path.join(directory, 'secrets.ini')))

logging.config.fileConfig(os.path.abspath(os.path.join(directory, 'logging.ini')))
logger = logging.getLogger('rb_network')

DEFAULT_USER = config['default_user']['user']
USERNAME = config[DEFAULT_USER]['user']
PASSWORD = config[DEFAULT_USER]['password']

if __name__ == '__main__':
    print("Non dovresti essere qui...")
