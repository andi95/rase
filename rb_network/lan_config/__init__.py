import re


def get_netbit(netmask):
    netmask="/"+netmask
    lookup_table = {
        '/24': "255.255.255.0",
        '/25': "255.255.255.128",
        '/26': "255.255.255.192",
        '/27': "255.255.255.224",
        '/28': "255.255.255.240",
        '/29': "255.255.255.248",
        '/30': "255.255.255.252",
    }
    return lookup_table[netmask]


def check_addr(param):
    import ipaddress
    addr = ipaddress.ip_address(param)
    if not (not addr.is_loopback and not addr.is_multicast \
            and not addr.is_private and not addr.is_reserved):
        raise AttributeError("{!s} does not appear to be a valid public server address".format(param))
    return param

def check_param(string: str):
    """
        Error checking della stringa, lunghezza, injection,
        validità e complessità della uid/pass e quant'altro
    :type string: str
    :rtype: bool
    :param string:
    :return:
    """
    flag = True
    # TODO: fare errro checking per controllora la validità
    if len(string) < 8:
        flag = False
        msg = "%r Stringa troppo corta!" % string
        raise ValueError(msg)
    param = re.sub("[^A-Za-z0-9_]", "", string)[:20]
    if param != string:
        flag = False
        msg = "%r Stringa non valida!" % string
        raise ValueError(msg)
    if flag is False:
        raise ValueError("Errore nella stringa")
    else:
        return param


