import socket
import struct

import time

import select

MCAST_GRP = '225.6.7.8'
MCAST_PORT = 2302


def main():
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind(('', MCAST_PORT))
    sock.setblocking(0)
    mreq = struct.pack("4sl", socket.inet_aton(MCAST_GRP), socket.INADDR_ANY)
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
    ready = select.select([sock], [], [], 5)
    if ready[0]:
        data = sock.recv(1024).decode()
        if 'Multicast test on VBS parameters finished successfully' in data:
            print(data)


if __name__ == '__main__':
    main()
