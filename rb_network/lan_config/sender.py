import socket

MCAST_GRP = '225.6.7.8'
MCAST_PORT = 2302

def main():
    print("Sent join message on group {MCAST_GRP} on port {MCAST_PORT}. Check on server if successful.".format(**globals()))
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 2)
    sock.sendto(b"Multicast test on VBS parameters finished successfully", (MCAST_GRP, MCAST_PORT))

if __name__ == '__main__':
    main()
