""" File che rappresenta il Cloud Core gateway

    La classe rappresentativa del Cloud Core sarà una classe che avrà le seguenti interfacce:
        +
        |
        +--> 20 interfacce eoip
        |
        +--> 1 interfaccia bridge (forse dovrò aggiungerne un'altra)
        |
        +--> 8 interfacce ethernet
        |
        +--> FUTURE INTERFACCE CHE DEVONO ESSERE AGGIUNTE
        
    Per quanto riguarda le 20 interfacce EoIp non vengono aggiunte hard-coded ma saranno aggiunte sotto forma @property 
    a run-time e userò un metodo singolo per la gestione delle varie interfacce visto che sono molto simili fra loro
    L'interfaccia bridge invece è molto importante perchè mi permette di avere il traffico broadcast "riflesso" su tutte
    le interfacce ethernet e anche quele virtuali quali VPN e le EoIp.
    
    Dovrò prevedere altri metodi di gestione dlle route inq uanto per ora le gestisco a mano ma in futuro dovrò usare
    dei protocolli di route redistribution come OSPF insieme a BGP in modo da liberarmi e averre anche gli aggiornamenti
    in tempo reale per quanto riguarda le variazioni di topologia di rete.
    
    Per prevedere tutto questo userò un Bridge Pattern che mi permetterà di usare fino ad ora una gestion delle route
    (per esempio) statica e poi quando faremo uno switch verso protocolli di redistribuzione dinamici allora potrò anche
    cambiare molto semplicemente l'implementazione della route management. Class Layout:
    
    +---------------+                +---------------+                  USER è l'implementatore che lo userà, può essere
    |      CCR      |                | ROUTE MANAGER |                  usato importandolo (quindi come libreria) oppure
    +---------------+                +---------------+                  lo istanzio direttamente dal __main__ del script
    |LIB/STAND-ALONE+---------------^+               |
    +---------------+                +-------+-------+                  ROUTE MANAGER  è a classe che serve ad astrarre 
                                             ^                          l'implementazion e mi permette di usare in mani-
                                             |                          era intercambiabile le due implementazioni delle
                                 +-----------+-----------+              route manager, posso usare un first class object
                                 |                       |              function invece di una classe senza l'overhead
                                 |                       |
                             +---+--------+      +-------+-----+        STATIC ROUTE è l'implementazione della gestione 
                             |STATIC ROUTE|      |DYNAMIC ROUTE|        statica delle varie route, fatta cioè a mano con
                             +------------+      +-------------+        route inserite in via programmatica via python
                             +------------+      +-------------+        
                                                                        DYNAMIC ROUTE invece è l'oppposto della gestione
                                                                        statica delle route, cioè mi affido a protocolli
                                                                        dinamici come OSPF insieme BGP tutto automatico
    
    USO UNO STRATEGY PATTERN NON PER UN SEMPLICE ESERCIZIO DI STILE (FORNIRE AL CLIENT CHE LO USA UN'INTERFACCIA CHE NON
    SA NIENTE DI COME SI CHIAMANO GLI OGGETTI) MA SOPRATUTTO PER POTER INCAPSULARE IL TUTTO IN UNA SERIE DI TASK MANAGER
    IN MODO DA AVERE UNA PROGRAMMAZIOEN async GRAZIE ALLA LIBRERIA ASYNCIO DI PYTHON 3.5 (da implementare!!)


"""
import argparse
import ipaddress
import sys
sys.path.append("/ro/usr/local/bin/rb_network")
import textwrap
import time
from concurrent.futures import ThreadPoolExecutor
from subprocess import Popen, PIPE
import queue
import threading, queue

import subprocess

from api_file.context_api import Context_Api, ADDRESS
from lan_config import check_addr, check_param, get_netbit
from main import Mikrotik_Interface

operations = []


class Cloud_Core:
    """
    Classe rappresentativa del Cloud Core, segue listato delle varie interfacce
    Doctest example:
    >>> import time
    >>> time_t = time.time()
    >>> ccr = Cloud_Core()
    >>> print(getattr(ccr, 'ether4').address.address)
    RuntimeError: Address not found! Check your input
    >>> print(ccr.vpn_client_status)
    ['running=false', 'disabled=false']
    >>> print(ccr.vpn_server_status)
    enabled=false
    >>> print(ccr.vpn_status)
    Feedback Client not connected successfully
    >>> print("Time elapsed: {}".format(time.time() - time_t))
    Time elapsed: 1.4527792930603027
    """

    def __call__(self, *args, **kwargs):
        """
        Uso il Cloud_Core come una first-class object (approccio funzionale) per sveltire i tempi di reperimento delle
        informazioni, per ora 0.960 sec (YMMV)
        :param args:
        :param kwargs:
        :return:
        """
        q = args[1]
        with Context_Api(ADDRESS) as api:
            response = api.talk(args[0])
            if "!trap" in response[0]:
                raise RuntimeError("Command not executed {0}".format(str(response)))
            elif "!re" in response[0]:
                for item in response:
                    if item[0] != "!done":
                        try:
                            q.put(item[1])
                        except:
                            return item[1]
            elif "!done" in response[0]:
                pass

    def __init__(self):
        """
        Questo metodo di inizializzazione non è molto elegante in quanto non fa uso di descriptors, ma per averli dovrei
        creare overriding descriptor che richiederebbe a sua volta un'interfaccia organica e unificata nonché rigidamen
        te organizzata molto simile a una query di un database VERRÀ AGGIUNTO (prima o poi...)
        È una prima soluzione semplice anche se non molto elegante che permette però di evitare enormi porzioni di codi-
        ce inutili come ad esempio 42 @property inutilissimi, anche se li ho messi nelle altre classi, qui non mi serve
        un'interfaccia, questa è la classe utente non me ne faccio di niente di un'interfaccia rigida...
        """
        with Context_Api(ADDRESS) as api:
            response = api.talk(["/interface/print"])
            for item in response:
                if item[0] != '!done':
                    setattr(self, item[1]['=name'], Mikrotik_Interface(item))
        try:
            self.client_interface = [self.__getattribute__(name) for name, value
                                     in self.__dict__.items() if 'pptp-out' in value.type][0]
        except IndexError:
            raise RuntimeError("VPN client interface not found")
            # todo: Prevedere la creazione di un'interfaccia VPN

    @property
    def vpn_client_status(self):
        with Context_Api(ADDRESS) as api:
            response = api.talk(["/interface/pptp-client/print",
                                 "?.id={0}".format(self.client_interface.uid)])
            if "!trap" in response[0]:
                raise RuntimeError("Command not executed {0}".format(str(response)))
            elif "!re" in response[0]:
                return ["running={=running}".format(**response[0][1]),
                        "disabled={=disabled}".format(**response[0][1])]
            elif "!done" in response[0]:
                raise RuntimeError("Interface not found! Check your input")

    @vpn_client_status.setter
    def vpn_client_status(self, value):
        cmd = ""
        for key, value in value.items():
            cmd += "={0}={1}".format(key, value)
        with Context_Api(ADDRESS) as api:
            response = api.talk(["/interface/pptp-client/set",
                                 "=.id={0}".format(self.client_interface.uid),
                                 cmd])
            if "!trap" in response[0]:
                raise RuntimeError("Command not executed {0}".format(str(response)))

    @property
    def vpn_server_status(self):
        with Context_Api(ADDRESS) as api:
            response = api.talk(["/interface/pptp-server/server/print"])
            if "!trap" in response[0]:
                raise RuntimeError("Command not executed {0}".format(str(response)))
            elif "!re" in response[0]:
                return "enabled={=enabled}".format(**response[0][1])
            elif "!done" in response[0]:
                raise RuntimeError("Interface not found! Check your input")

    @vpn_server_status.setter
    def vpn_server_status(self, value):
        cmd = ""
        for key, value in value.items():
            cmd += "={0}={1}".format(key, value)
        with Context_Api(ADDRESS) as api:
            response = api.talk(["/interface/pptp-server/server/set", cmd])
            if "!trap" in response[0]:
                raise RuntimeError("Command not executed {0}".format(str(response)))

    @property
    def vpn_status(self):
        clientStatus = self.vpn_client_status
        serverStatus = self.vpn_server_status
        if serverStatus == 'enabled=false':
            if clientStatus[1] == 'disabled=true':
                return "choose"
            elif clientStatus == ['running=false', 'disabled=false']:
                return "Feedback Client not connected successfully"
            elif clientStatus == ['running=true', 'disabled=false']:
                return "client"
        elif serverStatus == 'enabled=true':
            if clientStatus[1] == 'disabled=true':
                return "server"
            elif clientStatus[1] == 'disabled=false':
                return "Feedback Error invalid VPN configuration, please choose a VPN mode."
            else:
                return "Feedback Error parsing response."

    @vpn_status.setter
    def vpn_status(self, value):
        """
        Setter della property status in cui;
            * vpn_client -> disabled
            * vpn_server -> enable
        Essendo opposti nel mikrotik vanno gestiti in maniera oppposta anche nel codice
        :param value:
        :return None:
        """
        if value == 'choose':
            self.vpn_client_status = {'disabled': 'true', }
            self.vpn_server_status = {'enabled': 'false', }
        elif value == 'server':
            self.vpn_client_status = {'disabled': 'true', }
            self.vpn_server_status = {'enabled': 'true', }
        elif value == 'client':
            self.vpn_client_status = {'disabled': 'false', }
            self.vpn_server_status = {'enabled': 'false', }
        else:
            raise ValueError("{0!s} Is not a correct setting".format(value))

    @classmethod
    def check_port(cls, address):
        # if ipaddress.ip_address(str(address)).is_global:
        process = Popen(["nc", "-zv", address, "1723"],
                        stdout=PIPE, stderr=PIPE)
        stdout, stderr = process.communicate(timeout=2)
        if b'1723 port [tcp/*] succeeded!' in stderr:
            return 'Port opened on server'
        else:
            return "Port closed on server"

    @property
    def connect_to(self):
        with Context_Api(ADDRESS) as api:
            response = api.talk(["/interface/pptp-client/print"])
            if "!trap" in response[0]:
                raise RuntimeError("Command not executed {0}".format(str(response)))
            elif "!re" in response[0]:
                return response[0][1]['=connect-to']
            elif "!done" in response[0]:
                raise RuntimeError("Error VPN interface not found")

    @connect_to.setter
    def connect_to(self, value):
        if Cloud_Core.check_port(value):
            with Context_Api(ADDRESS) as api:
                api.talk(["/interface/pptp-client/set",
                          "=.id={0!s}".format(self.client_interface.uid),
                          "=connect-to={0!s}".format(value)])


if __name__ == '__main__':
    time_t = time.time()
    parser = argparse.ArgumentParser(
        prog='LAN_Modifier',
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=textwrap.dedent('''\
                ...            Script LAN configuration modifier
                ...           --------------------------------
                ...             Scrit della manipolazione
                ...             della LAN e VPN (protocollo PPTP)
                ...         '''))

    parser.add_argument('-g', '--getparams',
                        action="store_true",
                        help="Ritrova i parametri connessione client")

    parser.add_argument('-G', '--getserverparams',
                        action="store_true",
                        help="Ritrova i parametri connessione server")

    parser.add_argument('-m', '--mode',
                        action="store_true",
                        help="Get dello status iniziale ON/OFF")

    parser.add_argument('-s', '--make_server',
                        help="Settaggi della VPN",
                        choices=['0', '1', '2'])

    parser.add_argument('-u', '--username',
                        help="username della VPN",
                        required=False,
                        type=check_param)

    parser.add_argument('-p', '--password',
                        help="Password della VPN",
                        required=False,
                        type=check_param)

    parser.add_argument('-c', '--connect_to',
                        help="Indirizzo pubblico del server",
                        required=False,
                        type=check_addr
                        )

    parser.add_argument('bar', nargs='*', help='garbage arguments',
                        )

    try:
        args = parser.parse_args()
    except Exception as e:
        sys.exit("Error in parameters {!s}".format(e))

    if args.getparams:
        q = queue.Queue()
        Cloud_Core.__call__(Cloud_Core, ["/interface/pptp-client/print"], q)
        dictionary1 = q.get(timeout=1)
        stringa = 'connect-to="{=connect-to}" name="{=user}" password="{=password}"'.format(**dictionary1)
        Cloud_Core.__call__(Cloud_Core, ["/ip/route/print", "?comment=user:{=user}".format(**dictionary1), ], q)
        if not q.empty():
            response = q.get(timeout=0.5)
            stringa += ' subnet="{0!s}"'.format(*response['=dst-address'].split("/"))
            stringa += ' netmask="{0!s}"'.format(get_netbit(response['=dst-address'].split("/")[1]))
        print(stringa)

    if args.getserverparams:
        stringa = ""
        q = queue.Queue()
        threading.Thread(target=Cloud_Core.__call__(Cloud_Core, ["/ppp/secret/print"], q)).start()
        while not q.empty():
            response = q.get()
            stringa += 'subnet="{0!s}" netmask="{0!s}"' \
                       ' user="{=name}" password="{=password}"_^_'. \
                format(response['=routes'].split("/")[0],
                       get_netbit(response['=routes'].split("/")[1]), **response)
        print(stringa)

    if args.mode:
        ccr = Cloud_Core()
        print(ccr.vpn_status)

    if args.make_server:
        if args.make_server == "1":
            args.make_server = "client"
        elif args.make_server == "0":
            args.make_server = "choose"
        elif args.make_server == "2":
            args.make_server = "server"
        ccr = Cloud_Core()
        ccr.vpn_status = args.make_server

    if args.username:
        ccr = Cloud_Core()
        ccr.vpn_client_status = {'user': args.username}
        print("User {!s} configured".format(args.username))


    if args.password:
        ccr = Cloud_Core()
        ccr.vpn_client_status = {'password': args.password}
        print("Password configured")

    if args.connect_to:
        with ThreadPoolExecutor(max_workers=1) as executor:
            future = executor.submit(Cloud_Core.check_port, args.connect_to)
            print(future.result())
        ccr = Cloud_Core()
        ccr.vpn_client_status = {'connect-to': args.connect_to}

# process = Popen(["python3", "connection.py", "1"],
#                     stdout=PIPE, stderr=PIPE)
#     stdout, stderr = process.communicate(timeout=5)
#     print(stderr.decode(), stdout.decode())
# print("Time elapsed: {!s}".format(time.time() - time_t))

