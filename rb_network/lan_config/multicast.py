"""File che si occupa del check della connessione multicast"""
import queue
import threading
import sys
sys.path.append("/ro/usr/local/bin/rb_network")
from api_file.wds_params import WDS
from lan_config.LAN_Config import Cloud_Core
from lan_config import receiver, sender


def printwds(q):
    wds = WDS()
    if wds.wds_mode == "on":
        q.put(wds.server_client)


def printvpn(q):
    ccr = Cloud_Core()
    q.put("vpn={!s}".format(ccr.vpn_status))


if __name__ == '__main__':
    q = queue.Queue()
    threading.Thread(target=printvpn(q)).start()
    threading.Thread(target=printwds(q)).start()
    wds_status = None
    while not q.empty():
        response = q.get()
        if response.startswith("wds"):
            wds_status = response
        if response.startswith("vpn"):
            vpn_status = response

    if wds_status == None:
        if vpn_status == "client":
            sender.main()
        else:
            receiver.main()
    elif vpn_status == "choose":
        if wds_status == "client":
            sender.main()
        else:
            receiver.main()
    else:
        print("WDS and VPN both active check your configuration")
