import abc
import ipaddress
from collections import namedtuple

import time

from api_file.context_api import ADDRESS, Context_Api


class Mikrotik_Device(object):
    """
    Classe che rappresenta l'access point nella sua interezza, con le funzionalità
    riguardanti la WDS un health check e l'identity
    CLASSE DI TEST
    """

    def __init__(self):
        with Context_Api(ADDRESS)as api:
            response = api.talk(["/interface/print"])
        for item in response:
            if item[0] == '!done':
                break
            interface = Mikrotik_Interface(item)
            setattr(self, interface.name, interface)


class Interface(metaclass=abc.ABCMeta):
    @property
    @abc.abstractmethod
    def uid(self):
        pass

    @property
    @abc.abstractmethod
    def address(self):
        pass

    @address.setter
    @abc.abstractmethod
    def address(self, value):
        pass

    @property
    @abc.abstractmethod
    def mac_address(self):
        pass

    @mac_address.setter
    @abc.abstractmethod
    def mac_address(self, value):
        pass


class Mikrotik_Interface(Interface):
    def __init__(self, response):
        response = response[1]
        self._uid = response['=.id']
        self._type = response['=type']
        try:
            self._name = response['=name']
        except KeyError:
            try:
                self._name = response['=default-name']
            except Exception as e:
                print(e)
        try:
            self._mac_address = response['=mac-address']
        except KeyError:
            self._mac_address = None
        with Context_Api(ADDRESS) as api:
            response = api.talk(["/ip/address/print", "?interface=" + self._name])
        if "!trap" in response[0]:
            raise RuntimeError("Command not executed {0}".format(str(response)))
        elif "!re" in response[0]:
            for item in response:
                if item[0] == '!done':
                    break
                self._address = Address(item)
        elif "!done" in response[0]:
            self._address = Address(0)

    @property
    def type(self):
        return self._type

    @property
    def mac_address(self):
        with Context_Api(ADDRESS) as api:
            response = api.talk(["/interface/print", "?.id=".format(self._uid)])
            if "!trap" in response[0]:
                raise RuntimeError("Command not executed {0}".format(str(response)))
            elif "!re" in response[0]:
                return response[0][1]['=mac-address']
            elif "!done" in response[0]:
                raise RuntimeError("Interface not found! Check your input")

    @property
    def address(self):
        return self._address

    @property
    def uid(self):
        return self._uid

    @property
    def name(self):
        with Context_Api(ADDRESS) as api:
            response = api.talk(["/interface/print", "?.id=".format(self._uid)])
            if "!trap" in response[0]:
                raise RuntimeError("Command not executed {0}".format(str(response)))
            elif "!re" in response[0]:
                return response[0][1]['=name']
            elif "!done" in response[0]:
                raise RuntimeError("Interface not found! Check your input")


class Address:
    Flags = namedtuple('Flags', 'invalid dynamic disabled')

    def __init__(self, response):
        try:
            response = response[1]
        except Exception:
            self._address = "0.0.0.0/0"
            self._network = "0.0.0.0"
            self._flags = self.Flags(invalid="true", dynamic="false", disabled="true")
            self._uid = "N/A"
            return
        try:
            address = ipaddress.ip_address(response["=address"].split("/")[0])
        except ipaddress.AddressValueError as e:
            print(e)
        except ValueError as e:
            print(str(e).strip("ValueError: "))
        if not address.is_private:
            raise ValueError("Address {0} does not seem to be private".format(str(address)))

        try:
            network = str(ipaddress.ip_network(response["=address"], False))
        except ipaddress.AddressValueError as e:
            print(e)
        except ValueError as e:
            print(str(e).strip("ValueError: "))
        # if response["=network"] != network.split("/")[0]:
        #     raise ipaddress.AddressValueError("Something wrong with address")
        self._flags = self.Flags(invalid=response["=invalid"], dynamic=response["=dynamic"],
                                 disabled=response["=disabled"])
        self._address = address
        self._network = network
        self._uid = response["=.id"]

    @property
    def address(self):
        with Context_Api(ADDRESS) as api:
            response = api.talk(["/ip/address/print", "?.id={}".format(self._uid)])
            if "!trap" in response[0]:
                raise RuntimeError("Command not executed {0}".format(str(response)))
            elif "!re" in response[0]:
                return response[0][1]['=address']
            elif "!done" in response[0]:
                raise RuntimeError("Address not found! Check your input")

    @address.setter
    def address(self, value):
        with Context_Api(ADDRESS) as api:
            response = api.talk(["/ip/address/set", "=.id={0}".format(self._uid), "=address={0}".format(value)])
            if "!trap" in response[0]:
                raise RuntimeError("Command not executed {0}".format(str(response)))
            elif "!re" in response[0]:
                raise RuntimeError("General error try rebooting the router")
            elif "!done" in response[0]:
                pass

    @property
    def network(self):
        with Context_Api(ADDRESS) as api:
            response = api.talk(["/ip/address/print", "?.id={}".format(self._uid)])
            if "!trap" in response[0]:
                raise RuntimeError("Command not executed {0}".format(str(response)))
            elif "!re" in response[0]:
                return response[0][1]['=network']
            elif "!done" in response[0]:
                raise RuntimeError("Address not found! Check your input")

    @property
    def flags(self):
        with Context_Api(ADDRESS) as api:
            response = api.talk(["/ip/address/print", "?.id={}".format(self._uid)])
            if "!trap" in response[0]:
                raise RuntimeError("Command not executed {0}".format(str(response)))
            elif "!re" in response[0]:
                return self.Flags(invalid=response[0][1]["=invalid"], dynamic=response[0][1]["=dynamic"],
                                  disabled=response[0][1]["=disabled"])
            elif "!done" in response[0]:
                raise RuntimeError("Address not found! Check your input")

    @property
    def uid(self):
        return self._uid

        # ..ellypsys... Prima c'era un __setattr__()


if __name__ == '__main__':
    ap = Mikrotik_Device()
    time_t = time.time()
    ap.ether2.address.address = "192.168.109.178/24"
    print(time.time() - time_t)
