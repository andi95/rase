from setuptools import setup

setup(name='RASE',
      version='0.1',
      description='Rebel Alliace synthetic environment',
      url='http://www.rebel-alliance.com/',
      author='Andi Dulla',
      author_email='adulla95@gmail.com',
      license='MIT',
      packages=['rb_network/','rb_network/AP_Modifier','rb_network/api_file','rb_network/Fusion_Server_Config',], #Elenc TUTTI I packages!!
      zip_safe=False)